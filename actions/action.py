# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions
import math
import pickle
import random
import smtplib
import urllib.parse
import webbrowser
import numpy as np
from requests_html import HTMLSession




questionone = ['If you point at something across the room, does your child look at it?  For example, if you point at a toy or an animal, does your child look at the toy or animal?',
             'Does your child play pretend or make-believe? For example, pretend to drink from an empty cup, pretend to talk on a phone or pretend to feed a doll or stuffed animal',
             'Does your child like climbing up on things?','Does your child point with one finger to ask for something or get help?',
             'Does your child point with one his/her fingers to show you something interesting?','Does your child show interest in interacting with other children?',
             'Does your child show you things by bringing them to you or holding them up for you to see',
             'Does your child engage in walking or other movement activities? For example: playing with toys or furniture']

questiontwo = ['When you smile at your child, does he/she smile back at you','Does your child try to copy what you do? For example, wave hands, clap or make funny noises',
               'If you turn your head to look at something, does your child look around to see what you are looking at ?',
               'Does your child like movement activities', 'Does your child look you in the eye when you are talking to him or her',
               'If something new happens, does your child look at your face','Does your child try to get you to watch him or her?','Does your child make unusual finger movements']



import spacy
import nltk
import sys
import pyrebase
nltk.download('words')
nltk.download('wordnet')
from nltk.corpus import words
from nltk.corpus import wordnet
from nltk.tokenize import TreebankWordTokenizer
from string import punctuation
from heapq import nlargest
import MySQLdb
import re
from newspaper import Article
from nltk.tokenize.punkt import PunktSentenceTokenizer
from spacy.lang.en.stop_words import STOP_WORDS
from nltk.corpus import stopwords
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import CountVectorizer
from rasa_sdk.events import SlotSet, EventType
from rasa_sdk.events import FollowupAction
import string
from rasa_sdk.types import DomainDict
from mysql.connector import Error
from typing import Any, Text, Dict, List, Union
from rasa.core.tracker_store import DialogueStateTracker
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

unique_key = sys.argv[1]

pickle.dump(unique_key, open("passwd.data", "wb"))
passwd = pickle.load(open("passwd.data", "rb"))



def DataConnect(host_name, root, password, db_name):
    mydb = None

    try:
        mydb = MySQLdb.connect(host_name, root, password, db_name)

    except Error as e:
        print(e)

    return mydb


mydb = DataConnect("localhost", "root", "Patel_4007", "rasa_input")

def Users(mydb, userName, email, password, Relation):
    mycursor = mydb.cursor()

    # sql = "CREATE TABLE Input_rasa (personName VARCHAR(255), relation VARCHAR(255), input VARCHAR(255));"

    sql = 'insert into users(userName, email password, relation) values ("{0}","{1}","{2}","{3}");'.format(
        userName, email, password, Relation)

    mycursor.execute(sql)

    mydb.commit()

    print(mycursor.rowcount, "record inserted")

    mydb.close()


def PatientData(mydb, email, PersonName, summary, motor_skills, ehcd, compulsive):
    mycursor = mydb.cursor()

    # sql = "CREATE TABLE Input_rasa (personName VARCHAR(255), relation VARCHAR(255), input VARCHAR(255));"

    sql = 'insert into patient_data(email, personName, summary, motor_skills, eye_hand_coord, compulsive) values ("{0}","{1}","{2}","{3}","{4}", "{5}");'. \
        format(email, PersonName, summary, motor_skills, ehcd, compulsive)

    mycursor.execute(sql)

    mydb.commit()

    print(mycursor.rowcount, "record inserted")

    mydb.close()


modelPath = "C:\\Users\\JAY PATEL\\Desktop"

questions = ['Is your child attentive and able to look at objects you point at ?',
             'Does your child like movement activities, for example: continuously climbing the stairs, hand-flapping or other repetitive motions ?']


config = {

  "apiKey": "AIzaSyD32GdIXwRw4JyEgVBkwfld1TuO5hjQAfE",
  "authDomain": "converse-97c93.firebaseapp.com",
  "projectId": "converse-97c93",
  "storageBucket": "converse-97c93.appspot.com",
  "messagingSenderId": "866347656284",
  "appId": "1:866347656284:web:7edea22f84709c2fb73299",
  "measurementId": "G-BNC9N9PZP0",
  "databaseURL": "https://converse-97c93-default-rtdb.firebaseio.com/"

}

firebase = pyrebase.initialize_app(config)

def clean_string(text):

    text = ' '.join([word for word in text if word not in string.punctuation])
    text = text.lower()
    text = ' '.join([word for word in nltk.word_tokenize(text) if word not in stopwords.words('english')])

    return text

def cosine_sim_vectors(vec1, vec2):
    vec1 = vec1.reshape(1, -1)
    vec2 = vec2.reshape(1, -1)

    return cosine_similarity(vec1, vec2)[0][0]


def DataQuery(mydb, query):

    cursor = mydb.cursor()

    cursor.execute(query)

    para1 = cursor.fetchall()

    cursor.close()

    return para1

class ActionHelloWorld(Action):

    def name(self) -> Text:
        return "action_hello_world"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        name = tracker.get_slot("first")

        nlp = spacy.load("en_core_web_sm")

        doc = nlp(str(tracker.latest_message['text']))

        for word in doc.ents:
            if len(name) > 2 and word.label_ == 'PERSON':

                n = word.text

                db = firebase.database()

                message = "nice to meet you {}".format(n)

                dispatcher.utter_message(text=message)
                dispatcher.utter_message(text="How are you related to the autistic individual, for example: are you a mother, father or brother")

                data = {"name": n}

                db.child("users").child("ownkey").set(data)

                return [SlotSet('first', n)]

class ActionRelation(Action):

    def name(self) -> Text:
        return "action_relation"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        w = tracker.get_slot("first")
        m = str(tracker.latest_message['text'])
        family_list = ['mother', 'father', 'brother', 'sister', 'grandfather', 'grandmother',
                       'teacher', 'guardian', 'cousin', 'friend', 'nephew', 'neice', 'uncle', 'aunt']

        s = re.findall(r"\w+", m)

        e = ""

        for w in s:
            for a in family_list:
                if w.lower() == a:
                    e = w.lower()
                    break

        dispatcher.utter_message(text="what is the name of your child who has autism issues ?")

        return [SlotSet('first',w),SlotSet('relation',e)]

class ActionPatient(Action):

    def name(self) -> Text:
        return "action_patient_name"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        e = tracker.get_slot('first')
        m = tracker.get_slot('relation')

        name = tracker.get_slot("patient")

        nlp = spacy.load("en_core_web_sm")

        doc = nlp(str(tracker.latest_message['text']))

        for word in doc.ents:
            if len(name) > 2 and word.label_ == 'PERSON':

                n = word.text

                dispatcher.utter_message(response="utter_mail")


                return [SlotSet('patient', n), SlotSet('first', e), SlotSet('relation',m)]

class ActionOTP(Action):

    def name(self) -> Text:
        return "action_otpwrd"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        b = tracker.get_slot("email")
        w = tracker.get_slot("otpwd")

        m = str(tracker.latest_message['text'])

        query = "select password from users where email = '{0}';".format(b)

        wp = DataQuery(mydb, query)

        output = ''.join([''.join(map(str, tup)) for tup in wp])

        if (m == w and len(output) > 0):

            dispatcher.utter_message(text="your account password is " + output)
            dispatcher.utter_message(text="type 'login' to sign in your account")


        return [SlotSet("email", b)]

class ActionPasswrd(Action):

    def name(self) -> Text:
        return "action_passwrd"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:


        e = tracker.get_slot("email")
        patient_name = tracker.get_slot("patient")
        name = tracker.get_slot("first")
        relations = tracker.get_slot("relation")

        f = str(tracker.latest_message['text'])

        query = "select email from users where email = '{0}';".format(e)

        wp = DataQuery(mydb, query)

        queries = "select password from users where password = '{0}';".format(f)

        p = DataQuery(mydb, queries)

        output = ''.join([''.join(map(str, tup)) for tup in wp])

        outputs = ''.join([''.join(map(str, tup)) for tup in p])

        characters = "0123456789abcdefghijklmnopkrstuvwxyz"


        if (len(output) > 0 and len(outputs) > 0):

            dispatcher.utter_message("Welcome")

        elif(len(output) > 0 and len(outputs) == 0):

            dispatcher.utter_message("invalid password")

            s = smtplib.SMTP('smtp.gmail.com', 587)

            s.starttls()

            s.login("patel0047700@gmail.com", "P/t&l_7004")

            otp = ""

            length = len(characters)

            for i in range(5):
                otp += characters[math.floor(random.random() * length)]

            message = "\nHello, here is your otp: {}".format(otp)

            s.sendmail("patel41100@gmail.com", output, message)

            s.quit()

            dispatcher.utter_message(text="An email has been sent to you. enter the otp received to access the password")

            return [SlotSet("otpwd", otp), SlotSet("email", e)]


        elif (len(outputs) == 0 and len(output) == 0):

            Users(mydb, name, e, f, relations)
            PatientData(mydb, e, patient_name, "", "", "", "")

            dispatcher.utter_message("account created successfully")


        else:
            dispatcher.utter_message("wrong input")
            dispatcher.utter_message("type 'sign up' to retry login")



        return [SlotSet('email', e)]



class ActionMail(Action):

    def name(self) -> Text:
        return "action_mail"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        f = tracker.get_slot('first')
        y = tracker.get_slot('relation')
        d = tracker.get_slot('patient')

        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'

        digits = "0123456789"

        name = tracker.get_slot("email")

        m = str(tracker.latest_message['text'])

        lsm = re.findall(regex, m)

        v = ''.join(lsm)

        if len(name) > 0 and len(lsm) == 1:

            query = "select email from users where email = '{0}';".format(v)

            wp = DataQuery(mydb, query)

            output = ''.join([''.join(map(str, tup)) for tup in wp])

            if len(output) > 0:

                dispatcher.utter_message(text="Enter the password")

                return [SlotSet("email", output)]

            else:

                dispatcher.utter_message(text="email id doesn't exist in our database")

                dispatcher.utter_message(text="Enter a password to create an account")

                return [SlotSet("email", v), SlotSet('patient', d), SlotSet('first', f), SlotSet('relation', y)]
        else:

            dispatcher.utter_message(text="enter a valid email address")
            dispatcher.utter_message(response="utter_mail")



class ActionCustomFallback(Action):

    def name(self) -> Text:
        return "action_custom_fallback"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: DialogueStateTracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        users = tracker.get_slot('email')

        m = tracker.latest_message['text']
        u = tracker.get_latest_entity_values("check_similarity")
        v = list(u)
        entities_to_process = []

        entities_to_process.extend(v)

        return[SlotSet('email', users), FollowupAction("action_similarity")]

class ActionAttention(Action):

    def name(self) -> Text:
        return "action_attention"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[EventType]:

        users = tracker.get_slot("email")

        m = str(tracker.latest_message['entities'])

        if m == "similarity":

            cursor = mydb.cursor()

            while cursor.nextset() is not None: pass

            query = "update patient_data set motor_skills = concat(ifnull(motor_skills,''),'fail') where email = '{0}';".format(users)
            cursor.execute(query)
            mydb.commit()
            print("row updated")

        elif m == "compulsive":

            cursor = mydb.cursor()

            while cursor.nextset() is not None: pass

            query = "update patient_data set compulsive = concat(ifnull(compulsive,''),'fail') where email = '{0}';".format(users)

            cursor.execute(query)
            mydb.commit()
            print("row updated")

        else:
            cursor = mydb.cursor()

            while cursor.nextset() is not None: pass

            query = "update patient_data set eye_hand_coord = concat(ifnull(eye_hand_coord,''),'fail') where email = '{0}';".format(users)

            cursor.execute(query)
            mydb.commit()
            print("row updated")

        e = str(tracker.latest_message['text']) + "."

        nlp = spacy.load('en_core_web_sm')

        m = nlp(e)

        doc = nlp(' '.join([str(t) for t in m if not t.is_stop]))



        sims = []
        for token in questionone:
            sims.append(doc.similarity(nlp(' '.join([str(t) for t in nlp(token) if not t.is_stop]))))

        id_max = np.argmax(sims)

        dispatcher.utter_message(text=questionone[id_max],
                                     buttons=[
                                         {"title": "yes", "payload": "/affirm"},
                                         {"title": "no", "payload": "/deny"}
                                     ])

        return []

    def validate_attention(
            self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict) -> Union[Dict[str, bool], Dict[str, None]]:

        if tracker.get_intent_of_latest_message() == "affirm":
            return {"attention": True}
        if tracker.get_intent_of_latest_message() == "deny":
            return {"attention": False}
        dispatcher.utter_message(text="I didn't get that")
        return {"attention": None}


class LoginAction(Action):

    def name(self) -> Text:
        return "action_login"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        dispatcher.utter_message(text="Hi, I am Rasa. Login or Create Account",
                                 buttons=[
                                     {"title": "login", "payload": "/login"},
                                     {"title": "create account", "payload": "/create_account"}
                                 ])

        return []


class Similarity(Action):


    def name(self) -> Text:
        return "action_similarity"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        user = tracker.get_slot('email')


        nlp = spacy.load("en_core_web_sm")

        many_words = words.words() + wordnet.words()

        validate_events = []


        tokenizer = TreebankWordTokenizer()

        user_text = tracker.latest_message['text']

        tokenized_list = tokenizer.tokenize(user_text)

        for x in tokenized_list:
            if x not in many_words:
                validate_events.append(x)

        if len(validate_events) > 0:
            dispatcher.utter_message(text="Sorry, I didn't understand Try Again")


        e = str(tracker.latest_message['text']) + "."
        m = nlp(e)


        if len(user_text) > 2:

            sims = []
            for token in nlp.pipe(questions):
                sims.append(m.similarity(token))

            id_max = np.argmax(sims)

            dispatcher.utter_message(text="Thanks for the input. You can access the summary by typing 'I want summary'")

        cursor = mydb.cursor()

        while cursor.nextset() is not None: pass
        querys = "update patient_data set summary = concat(summary, '{0}') where email = '{1}';".format(m, user)
        cursor.execute(querys)
        mydb.commit()
        print("row updated")


        return [SlotSet('email', user), FollowupAction("action_summary")]



class ActionResponse(Action):

    def name(self) -> Text:
        return "action_response"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[EventType]:

        use_email = tracker.get_slot("email")

        max_question = random.choice(questiontwo)


        if tracker.get_slot("attention") == True:
            dispatcher.utter_message(text="Do you find any type of different behaviour displayed by your kid ?")

        elif tracker.get_slot("attention") == False:


            dispatcher.utter_message(text=max_question,
                                     buttons=[
                                         {"title": "yes", "payload": "/affirm"},
                                         {"title": "no", "payload": "/deny"},
                                     ])
        else:
            dispatcher.utter_message(text = "No proper information")


        return []

    def validate_response(
            self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict) -> Union[Dict[str, bool], Dict[str, None]]:

        if tracker.get_intent_of_latest_message() == "affirm":
            return {"response": True}
        if tracker.get_intent_of_latest_message() == "deny":
            return {"response": False}
        dispatcher.utter_message(text="I didn't get that")
        return {"response": None}


class ActionFinal(Action):

    def name(self) -> Text:
        return "action_final_output"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        if tracker.get_slot("response") == True:
            dispatcher.utter_message(text="Tell me more about your kid's behaviour and activities")

        elif tracker.get_slot("response") == False:
            dispatcher.utter_message(text="your child might be having an early symptom of autism. Consulting a health professional will help")
        else:
            dispatcher.utter_message(text="No proper information")

        return []


class ActionSummary(Action):

    def name(self) -> Text:
        return "action_summary"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        uses = tracker.get_slot('email')


        stopwords = list(STOP_WORDS)


        query = "select summary from patient_data where email = {0};".format(uses)

        wp = DataQuery(mydb, query)

        output = ''.join([''.join(map(str, tup)) for tup in wp])

        nlp = spacy.load('en_core_web_sm')


        doc = nlp(str(output))

        word_dictionary = {}

        for word in doc:
            if word.text.lower() not in stopwords:
                if word.text.lower() not in punctuation:
                    if word.text not in word_dictionary.keys():
                        word_dictionary[word.text] = 1
                    else:
                        word_dictionary[word.text] += 1

        max_frequency = max(word_dictionary.values())

        for word in word_dictionary.keys():
            word_dictionary[word] = word_dictionary[word]/max_frequency

        sentence_tokens = [sent for sent in doc.sents]

        sentence_scores = {}
        for sent in sentence_tokens:
            for word in sent:
                if word.text.lower() in word_dictionary.keys():
                    if sent not in sentence_scores.keys():
                        sentence_scores[sent] = word_dictionary[word.text.lower()]
                    else:
                        sentence_scores[sent] += word_dictionary[word.text.lower()]

        select_length = int(len(sentence_tokens)*0.5)
        summaries = nlargest(select_length, sentence_scores, key = sentence_scores.get)
        final_summary = [word.text for word in summaries]
        summary = ' '.join(final_summary)
        e = "autism treatment " + summary
        medical_reply = response(e)
        y = "home remedies " + summary + " autism"
        temperory_solution = response(y)

        dispatcher.utter_message(text="summary output: " + summary + "\n\n" + medical_reply + "\n\n" + temperory_solution)



        return []

def index_sort(list_var):

    length = len(list_var)
    list_index = list(range(0,length))

    x = list_var
    for i in range(length):
        for j in range(length):
            if x[list_index[i]] > x[list_index[j]]:

                list_index[i],list_index[j] = list_index[j],list_index[i]

    return list_index


def response(user_input):

    user_input = '+'.join(user_input.split())

    article = Article(webbrowser.get('google'), user_input)
    article.download()
    article.parse()
    article.nlp()
    corpus = article.text
    text = corpus
    text = text.translate(str.maketrans('', '', string.punctuation))
    sent_tokenizer = PunktSentenceTokenizer(text)
    sentence_list = sent_tokenizer.tokenize(text)  # nltk.sent_tokenize(text)

    user_input = user_input.lower()
    response = ''

    count_matrix = CountVectorizer(stop_words=["are", "all", "in", "and"]).fit_transform(
        sentence_list)  # (number of articles, number of unique words)
    array = count_matrix.toarray()
    similarity_matrix = cosine_similarity(count_matrix[-1], count_matrix)
    sparse_array = similarity_matrix.flatten()
    index = index_sort(sparse_array)
    index = index[1:]
    response_flag = 0
    j = 0

    for i in range(len(index)):
        if sparse_array[index[i]] > 0.0:
            sentence = ' '.join([str(element) for element in sentence_list])
            response = response + sentence
            response_flag = 1
            j = j + 1

        if response_flag == 0:
            response = response + ' ' + 'I did not understand'

        sentence = ' '.join([str(element) for element in sentence_list])

        response = ' '.join(response.split()[:32])

        b = urllib.parse.quote_plus(response)

        article = ''

        session = HTMLSession()

        r = session.get("https://www.google.com/search?q=" + b)

        links = list(r.html.absolute_links)

        google_domains = (
            'https://www.google.',
            'https://google.',
            'https://webcache.googleusercontent.',
            'http://webcache.googleusercontent.',
            'https://policies.google.',
            'https://support.google.',
            'https://maps.google.')

        for url in links[:]:
            if url.startswith(google_domains):
                links.remove(url)

        m = links[0]
        m = str(m)
        a = Article(m)
        a.download()
        a.parse()
        a.nlp()

        w = a.summary
        q = w + "\n\n" + "more information is available on this website: " + "\n" + m

        return q

    print('\n')